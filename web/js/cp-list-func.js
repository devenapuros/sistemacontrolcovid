$(document).ready( function () {

    edit_cp = '';

    $('#create-btn').click( function(event) {
        
        let facultad = $('#cp_facultad').val();
        let descripcion = $('#cp_descripcion').val();

        $.ajax({
            type:"POST",
            url:"create-cp.php",
            data: {
                facultad,
                descripcion
            },
            success: function (data) {
                let cps = JSON.parse(data);
                $('#table-body').html('');
                cps.forEach(element => {
                    $('#table-body').append(`
                    <tr>
                        <td scope="row">${element.id_control_point}</td>
                        <td>${element.facultad}</td>
                        <td>${element.descripcion}</td>
                        <td>
                            <a class="btn btn-success" href="punto-control.php?id_cp=${element.id_control_point}">VER</a>
                        </td>
                        <td>
                            <button class="edit-btn btn btn-primary" id="">EDITAR</a>
                        </td>
                        <td>
                            <button class="delete-btn btn btn-danger" id="">ELIMINAR</a>
                        </td>
                    </tr>
                   `); 
                });
            }
        })
        $('#newModal').modal('hide');
    });

    $('#newModal').on('hidden.bs.modal', function (event) {
        $('#newCp_descripcion').val('');
    });

    $('.edit-btn').click( function (event) {
        let id_cp = $(this).parent().parent().children()[0].innerHTML;
        $.ajax({
            type:"POST",
            url:"edit-cp-get.php",
            data: {
                id_cp
            },
            success: function (data) {
                let cp = JSON.parse(data);
                edit_cp = id_cp;
                $('#edit_descripcion').val(cp[0].descripcion);
                $('#editModal').modal('show');
            }
        });
    });

    $('#edit-btn').click( function(event) {
        
        let descripcion = $('#edit_descripcion').val();

        $.ajax({
            type:"POST",
            url:"edit-cp-update.php",
            data: {
                edit_cp,
                descripcion
            },
            success: function (data) {
                let cps = JSON.parse(data);
                $('#table-body').html('');
                cps.forEach(element => {
                    $('#table-body').append(`
                    <tr>
                        <td scope="row">${element.id_control_point}</td>
                        <td>${element.facultad}</td>
                        <td>${element.descripcion}</td>
                        <td>
                            <a class="btn btn-success" href="punto-control.php?id_cp=${element.id_control_point}">VER</a>
                        </td>
                        <td>
                            <button class="edit-btn btn btn-primary" id="">EDITAR</a>
                        </td>
                        <td>
                            <button class="delete-btn btn btn-danger" id="">ELIMINAR</a>
                        </td>
                    </tr>
                   `); 
                });
            }
        })
        $('#editModal').modal('hide');
    });

    $('.delete-btn').click( function (event) {
        let id_cp = $(this).parent().parent().children()[0].innerHTML;
        $.ajax({
            type:"POST",
            url:"delete-cp.php",
            data: {
                id_cp
            },
            success: function (data) {
                let cps = JSON.parse(data);
                $('#table-body').html('');
                cps.forEach(element => {
                    $('#table-body').append(`
                    <tr>
                        <td scope="row">${element.id_control_point}</td>
                        <td>${element.facultad}</td>
                        <td>${element.descripcion}</td>
                        <td>
                            <a class="btn btn-success" href="punto-control.php?id_cp=${element.id_control_point}">VER</a>
                        </td>
                        <td>
                            <button class="edit-btn btn btn-primary" id="">EDITAR</a>
                        </td>
                        <td>
                            <button class="delete-btn btn btn-danger" id="">ELIMINAR</a>
                        </td>
                    </tr>
                   `); 
                });
            }
        });
    });
});