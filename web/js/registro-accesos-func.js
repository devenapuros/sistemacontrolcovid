$(document).ready( function () {

    $('#search-btn').click( function (event) {
        let search = $('#search-input').val();
        if (search != '') {
            $.ajax({
                type:"POST",
                url:"search.php",
                data: {search},
                success: function (data) {
                    let search_res = JSON.parse(data);
                    console.log(search_res);
                    $('#table-body').html('');
                    search_res.forEach(element => {
                        $('#table-body').append(`
                        <tr>
                            <td>${element.id_persona}</td>
                            <td>${element.nombre}</td>
                            <td>${element.rol}</td>
                            <td>${element.descripcion}</td>
                            <td>${element.facultad}</td>
                            <td>${element.registed_at}</td>
                        </tr>
                   `); 
                });
                }
            })
        }
    })
})