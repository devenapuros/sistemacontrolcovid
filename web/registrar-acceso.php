<?php
    include('database.php');
    $id_persona = $_POST['id_p'];
    $id_cp = $_POST['id_cp'];

    $records = $connection->prepare('INSERT INTO acceso (id_persona,id_control_point) VALUES (:id_persona,:id_cp);');
    $records->bindParam('id_persona',$id_persona);
    $records->bindParam('id_cp',$id_cp);
    if ($records->execute()) {
        $records = $connection->prepare('SELECT id_ingreso,registed_at FROM acceso ORDER BY id_ingreso DESC LIMIT 1;');
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        $res = array("status" => 1, "registed_at" => $result['registed_at']);
        echo json_encode($res);
    }
    else{
        $res = array("status" => 0);
        echo json_encode($res);
    }
?>