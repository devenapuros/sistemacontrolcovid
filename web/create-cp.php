<?php
    include('database.php');
    $facultad = $_POST['facultad'];
    $descripcion = $_POST['descripcion'];

    $records = $connection->prepare('INSERT INTO control_points (descripcion,facultad) VALUES (:descripcion,:facultad);');
    $records->bindParam('descripcion',$descripcion);
    $records->bindParam('facultad',$facultad);
    $records->execute();

    $records = $connection->prepare('SELECT id_control_point FROM control_points ORDER BY id_control_point DESC LIMIT 1;');
    $records->execute();
    $result = $records->fetch(PDO::FETCH_ASSOC);

    $pathQR = 'qrcodes/'.$result['id_control_point'].'.png';
    $records = $connection->prepare('UPDATE control_points SET qrPath = :pathQR WHERE id_control_point = :id_cp');
    $records->bindParam('pathQR',$pathQR);
    $records->bindParam('id_cp',$result['id_control_point']);
    $records->execute();

    $records = $connection->prepare('SELECT id_control_point,descripcion,facultad FROM control_points;');
    $records->execute();
    $result = $records->fetchAll();
    echo json_encode($result);
?>